import { ISeedEntry } from '../contracts';

const data : ISeedEntry[] = [
{name: 'Marto', strength: 3, vitality: 100},
{name: 'Steven', strength: 4, vitality: 100},
{name: 'Roskata', strength: 5, vitality: 100},
{name: 'Petq', strength: 6, vitality: 100},
{name: 'Nadq', strength: 7, vitality: 100},
{name: 'Viktor', strength: 6, vitality: 100},
{name: 'Edward', strength: 5, vitality: 100},
{name: 'Doncho', strength: 4, vitality: 100},
{name: 'Aroyo', strength: 3, vitality: 100}
];

export {
  data
};
