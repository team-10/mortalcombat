import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICharacterDatabase, IOpponent, IPlayer, ISeedEntry } from '../contracts';
import { IDice } from '../contracts/providers/IDice';
import { Opponent } from '../models';

@injectable()

export class CharacterDatabase implements ICharacterDatabase {

  private _opponents: IOpponent[];

  public constructor(
  ) {
    this._opponents = [];
  }
  public get opponents(): IOpponent[] {
    return this._opponents;
  }
}
