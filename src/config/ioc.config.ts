import { Container, interfaces } from 'inversify';
import { ICharacterDatabase, ICommand, ICommandFactory,
   ICommandProcessor, IDependencyFactory, IDice, IEngine,
   IFightRound, IOpponentsFactory, IOptionsRound, IPlayer, IReader, IStartStage, IState, IWriter } from '../contracts';

import { CharacterDatabase } from '../database';
import { CommandProcessor, ConsoleReader, ConsoleWriter,
   DependencyFactory, Dice, FightRound, OpponentsFactory, OptionsRound, StartStage, State } from '../implementations';

import { Attack, Cast, Choose, Create, Pick, Rest } from '../commands';
import { Engine } from '../implementations/engine';
import { ContainerCommandFactory } from '../implementations/factories/container-command-factory';
import { Player } from '../models';
import { TYPES } from './types';

const container: Container = new Container();

// Engines
container.bind<IEngine>(TYPES.engine).to(Engine);
container.bind<IStartStage>(TYPES.startStage).to(StartStage);
container.bind<IFightRound>(TYPES.fightRound).to(FightRound);
container.bind<IOptionsRound>(TYPES.optionsRound).to(OptionsRound);

// Factories
container.bind<IOpponentsFactory>(TYPES.opponentsFactory).to(OpponentsFactory);
container.bind<IDependencyFactory>(TYPES.dependencyFactory).to(DependencyFactory);
container.bind<ICommandFactory>(TYPES.commandFactory).to(ContainerCommandFactory);
container
  .bind<interfaces.Factory<ICommand>>(TYPES.containerCommandFactory)
  .toFactory<ICommand>((context: interfaces.Context) =>
  (commandName: string): ICommand => context.container.get<ICommand>(TYPES[commandName]));

// Providers
container.bind<ICommandProcessor>(TYPES.commandProcessor).to(CommandProcessor);
container.bind<IDice>(TYPES.dice).to(Dice);
container.bind<IReader>(TYPES.reader).to(ConsoleReader);
container.bind<IWriter>(TYPES.writer).to(ConsoleWriter);
container.bind<IState>(TYPES.state).to(State).inSingletonScope();

container.bind<ICharacterDatabase>(TYPES.database).to(CharacterDatabase).inSingletonScope();
container.bind<IPlayer>(TYPES.player).to(Player);

// Commands
container.bind<ICommand>(TYPES.attack).to(Attack);
container.bind<ICommand>(TYPES.cast).to(Cast);
container.bind<ICommand>(TYPES.choose).to(Choose);
container.bind<ICommand>(TYPES.create).to(Create);
container.bind<ICommand>(TYPES.pick).to(Pick);
container.bind<ICommand>(TYPES.rest).to(Rest);

export { container };
