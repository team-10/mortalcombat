import { ITypes } from '../contracts/ITypes';

export const TYPES: ITypes = {
  // Engines
  engine: Symbol.for('engine'),
  startStage: Symbol.for('startStage'),
  fightRound: Symbol.for('fightRound'),
  optionsRound: Symbol.for('optionsRound'),

  // Factories
  dependencyFactory: Symbol.for('dependencyFactory'),
  commandFactory: Symbol.for('commandFactory'),
  containerCommandFactory: Symbol.for('commandContainerFactory'),
  opponentsFactory: Symbol.for('opponentsFactory'),

  // Providers
  commandProcessor: Symbol.for('commandProcessor'),
  dice: Symbol.for('dice'),
  reader: Symbol.for('reader'),
  writer: Symbol.for('writer'),
  state: Symbol.for('state'),

  database: Symbol.for('database'),
  player: Symbol.for('player'),

  // Commands
  attack: Symbol.for('attack'),
  cast: Symbol.for('cast'),
  choose: Symbol.for('choose'),
  create: Symbol.for('create'),
  pick: Symbol.for('pick'),
  rest: Symbol.for('rest')
};
