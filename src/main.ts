
// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { container } from './config/ioc.config';
import { TYPES } from './config/types';
import {  IEngine } from './contracts';

const engine: IEngine = container.get<IEngine>(TYPES.engine);
engine.start();
