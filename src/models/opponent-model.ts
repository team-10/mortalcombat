import { IOpponent } from '../contracts';
import { Character } from './character-model';

export class Opponent extends Character implements IOpponent {
  protected static count: number = 0;
  private _id: number;
  constructor (
    name: string,
    strength: number,
    vitality: number,
    inteligence: number
  ) {
    super(name, strength, vitality);
    Opponent.count += 1;
    this.id = Opponent.count;
    this.inteligence = inteligence;
  }
  public get id(): number {
    return this._id;
  }
  public set id(id: number) {
    this._id = id;
  }

  protected enhancement(): number {
    return this.strength + this.id;
  }
}
