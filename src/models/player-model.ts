import { IPlayer } from '../contracts';
import { Character } from './character-model';

export class Player extends Character implements IPlayer {
  constructor(name: string, strength: number, vitality: number) {
    super(name, strength, vitality);
    this.inteligence = 4;
  }

  public increaseStrength(value: number): void {
    if (value < 0) {
      throw new Error('IncreaseStrength only accepts positive number as parameter.');
    }
    this.strength += value;
  }
  protected enhancement(): number {
    return this.strength * 3;
  }
}
