import { injectable } from 'inversify';
import { ICharacter } from '../contracts';
@injectable()
export abstract class Character implements ICharacter {

  private _minDmg: number = 2;
  private _maxDmg: number = 6;
  private _inteligence: number;
  private _name: string;
  private _vitality: number;
  private _strength: number;

  protected constructor(name: string, strength: number, vitality: number) {
    if (name === '' || name.length > 15 || name.length < 2) {
      throw new Error(`Player's name should be between 3 and 16 symbols long!`);
    }
    this._name = name;
    this.strength = strength;
    this.vitality = vitality;
  }
  public get name(): string {
    return this._name;
  }
  public get strength(): number {
    return this._strength;
  }
  public set strength(value: number) {
    if (value < 1) {
      throw new Error(`Player's strength should be at least one.`);
    }
    this._strength = value;
  }
  public get inteligence(): number {
    return this._inteligence;
  }
  public set inteligence(value: number) {
    if (value < 1) {
      throw new Error(`Player's inteligence should be greater than 1!`);
    }
    this._inteligence = value;
  }
  public get vitality(): number {
    return this._vitality;
  }
  public set vitality(value: number) {
    if (value > 100 || value < 0) {
      throw new Error(`Player's vitality should be between 0 and 100!`);
    }
      this._vitality = value;
  }

  public get minDmg(): number {
    return this._minDmg;
  }
  public get maxDmg(): number {
    return this._maxDmg;
  }

  public spellDmg(): number {
    return this._inteligence * this._inteligence + this.enhancement();
  }

  public canCast(): boolean {
    return this.inteligence >= 5;
  }

  public isAlive(): boolean {
    return this.vitality > 0;
  }
  public toString(): string {
    return `${this.name}(s:${this.strength})(i:${this.inteligence})(v:${this.vitality}/100)`;
  }
  protected abstract enhancement(): number;

}
