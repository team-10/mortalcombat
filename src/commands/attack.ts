import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICharacter, ICharacterDatabase, ICommand, IDependencyFactory, IOpponent, IPlayer, IState, IWriter } from '../contracts';
import { IDice } from '../contracts/providers/IDice';

@injectable()
export class Attack implements ICommand {
  private writer: IWriter;
  private dice: IDice;
  private player: IPlayer;
  private opponent: IOpponent;

  public constructor (
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.dependencyFactory) private dependencyFactory: IDependencyFactory,
    @inject(TYPES.database) private database: ICharacterDatabase
  ) {
    this.writer = this.dependencyFactory.createWriter();
    this.dice = this.dependencyFactory.createDice();
    this.player = this.state.player;
    this.opponent = this.state.currentOpponent;
  }

  public execute (): void {
    this.hit();
    if (this.opponent.isAlive() && this.player.isAlive()) {
      this.state.activeCommandNames = ['attack'];
      if (this.state.player.canCast()) {
        this.state.activeCommandNames.push('cast');
      }
    } else {
      this.state.activeCommandNames = ['choose', 'rest'];
    }
  }

  private hit(): void {
    const calcPlayerDamage: number = this.dice.roll(this.player.minDmg, this.player.maxDmg) + this.player.strength;
    const calcOppDamage: number = this.dice.roll(this.opponent.minDmg, this.opponent.maxDmg);

    if (calcPlayerDamage === this.player.maxDmg + this.player.strength) {
      this.decreaseVitality(this.opponent, calcPlayerDamage);
      this.writer.write(`\nYou have critically hit ${this.opponent.name} dealing ${calcPlayerDamage} damage!
While in shock he misses his chance to retaliate`);
    } else if (calcPlayerDamage !== this.player.minDmg + this.player.strength) {
      this.decreaseVitality(this.opponent, calcPlayerDamage);
      this.writer.write(`\nYou have hit your opponent dealing ${calcPlayerDamage} damage`);
      if (this.opponent.isAlive()) {
        this.decreaseVitality(this.player, calcOppDamage);
        this.writer.write(`\n${this.opponent.name} swings back and hits you for ${calcOppDamage} damage`);
      }
    } else {
      this.decreaseVitality(this.player, calcOppDamage);
      this.writer.write(`\nAs you try to attack you slip out of balance and miss your target.
${this.opponent.name} takes his chance and deals ${calcOppDamage} damage to you`);
    }
  }
  private decreaseVitality(victim: ICharacter, ammount: number): void {
    // Logic moved from character class
    if (victim.vitality - ammount <= 0) {
      victim.vitality = 0;
    } else {
      victim.vitality -= ammount;
    }
    // Logic moved from character class
  }

}
