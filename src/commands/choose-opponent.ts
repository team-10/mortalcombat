import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICharacterDatabase, ICommand, IDependencyFactory, IOpponent, IReader, IState, IWriter } from '../contracts';
import { IDice } from '../contracts/providers/IDice';

@injectable()
export class Choose implements ICommand {

  private dice: IDice;
  private writer: IWriter;
  public constructor (
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.dependencyFactory) private dependencyFactory: IDependencyFactory,
    @inject(TYPES.database) private database: ICharacterDatabase
  ) {
    this.dice = this.dependencyFactory.createDice();
    this.writer = this.dependencyFactory.createWriter();
  }

  public execute(): void {
    const opponents: IOpponent[] = this.database.opponents;
    let opponentsList: IOpponent[] = [];

    if (opponents.length < 5) {
      opponentsList = opponents;
    } else {
      while (opponentsList.length < 5) {
        const newOpponent: IOpponent = opponents[this.dice.roll(0, opponents.length - 1)];
        if (!opponentsList.includes(newOpponent)) {
          opponentsList.push(newOpponent);
        }
      }
    }
    this.state.selectedOpponents = opponentsList;

    this.writer.write(`\nPick your opponent:`);
    opponentsList.forEach((opponent: IOpponent, index: number) => {
      this.writer.write(`${index + 1}. ${opponent.toString()}`);
    });
    this.writer.write(``);
    this.state.activeCommandNames = ['pick'];
  }
}
