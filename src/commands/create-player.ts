import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICommand, IDependencyFactory, IPlayer, IState } from '../contracts';

@injectable()
export class Create implements ICommand {
  private _state: IState;
  private readonly _factory: IDependencyFactory;

  public constructor(
    @inject(TYPES.state) state: IState,
    @inject(TYPES.dependencyFactory) factory: IDependencyFactory
  ) {
    this._state = state;
    this._factory = factory;
  }

  public execute(name: string): void {
    const strength: number = 26;
    const vitality: number = 100;
    const player: IPlayer = this._factory.createPlayer(name, strength, vitality);
    this._state.player = player;
    this._state.activeCommandNames = ['choose'];
  }
}
