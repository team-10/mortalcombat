import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICommand, IDependencyFactory, IOpponent, IState } from '../contracts';

@injectable()
export class Pick implements ICommand {

  public constructor (
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.dependencyFactory) private dependencyFactory: IDependencyFactory
  ) {}

  public  execute(input: string): void {
    // Pottentially make with input number or name
      const opponentList: IOpponent[] = this.state.selectedOpponents;
      this.state.activeCommandNames = ['attack'];
      if (this.state.player.canCast()) {
        this.state.activeCommandNames.push('cast');
      }
      this.state.currentOpponent = opponentList[Number(input) - 1];
  }
}
