import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICharacter, ICharacterDatabase, ICommand, IDependencyFactory, IOpponent, IPlayer, IState, IWriter } from '../contracts';
import { IDice } from '../contracts/providers/IDice';

@injectable()
export class Cast implements ICommand {
  private dice: IDice;
  private player: IPlayer;
  private opponent: IOpponent;
  private writer: IWriter;

  public constructor (
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.dependencyFactory) private dependencyFactory: IDependencyFactory,
    @inject(TYPES.database) private database: ICharacterDatabase
  ) {
    this.writer = this.dependencyFactory.createWriter();
    this.dice = this.dependencyFactory.createDice();
    this.player = this.state.player;
    this.opponent = this.state.currentOpponent;
  }
  public execute (): void {
    this.cast();
    if (this.opponent.isAlive() && this.player.isAlive()) {
      this.state.activeCommandNames = ['attack', 'cast'];
    } else {
      this.state.activeCommandNames = ['choose', 'rest'];
    }
  }

  private cast (): void {
    const castAttempt: boolean = this.dice.roll(1, 10) <= this.player.inteligence;
    if (castAttempt) {
      const spellDmg: number = this.player.spellDmg() - this.opponent.inteligence;
      this.decreaseVitality(this.opponent, spellDmg);
      this.writer.write(`\nWhile going into deep trance, you start chanting ancient words.
Bright light eludes your hands and jolts ${this.opponent.name} dealing ${spellDmg} damage to him`);
    } else if (this.opponent.isAlive()) {
      // Choose if opponent will cast or hit.
      if (this.opponent.canCast() && !this.dice.roll(0, 4)) {
        const oppSpellDmg: number = this.opponent.spellDmg() - this.player.inteligence;
        this.decreaseVitality(this.player, oppSpellDmg);
        this.writer.write(`\nAs you try to remember the right words, ${this.opponent.name} starts laughing.
He takes a deep breath and blasts a huge fireball hitting you for ${oppSpellDmg} damage`);
      } else {
        const calcOppDamage: number = this.dice.roll(this.opponent.minDmg, this.opponent.maxDmg);
        this.decreaseVitality(this.player, calcOppDamage);
        this.writer.write(`\nWhile you mumble something under your nose, ${this.opponent.name} quickly attacks you
you for ${calcOppDamage} damage preventing any further action`);
      }
    }
  }
  private decreaseVitality(victim: ICharacter, ammount: number): void {
    // Logic moved from character class
    if (victim.vitality - ammount <= 0) {
      victim.vitality = 0;
    } else {
      victim.vitality -= ammount;
    }
    // Logic moved from character class
  }
}
