import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICommand, IDependencyFactory, IPlayer, IState, IWriter } from '../contracts';
import { IDice } from '../contracts/providers/IDice';

@injectable()
export class Rest implements ICommand {
  private player: IPlayer;
  private writer: IWriter;
  private dice: IDice;

  public constructor (
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.dependencyFactory) private dependencyFactory: IDependencyFactory
  ) {
    this.player = this.state.player;
    this.writer = this.dependencyFactory.createWriter();
    this.dice = this.dependencyFactory.createDice();
  }

  public execute (): void {
    this.writer.write(`\nRESTING...`);
    const restLimit: number = 5;
    const increasedVitality: number = this.player.vitality + this.dice.roll(5, 15);
    this.player.vitality = increasedVitality > 100 ? 100 : increasedVitality ;
    this.state.activeCommandNames = ['rest', 'choose']; // Pottentially use enumeration
    if (this.player.vitality === 100) {
      this.writer.write('\nYour vitality is already at full capacity.\n');
      this.state.restsInRow = 5;
      this.state.activeCommandNames = ['choose'];
    } else if (this.state.restsInRow + 1 === restLimit) {
      this.writer.write(`You have no more rests and it's now time to choose an opponent.\n`);
      this.state.activeCommandNames = ['choose'];
    } else {
      this.state.restsInRow += 1;
      this.writer.write(`Your vitality is now at ${this.player.vitality}.`);
      this.writer.write(`You have ${restLimit - this.state.restsInRow} rests left.\n`);
    }
  }
}
