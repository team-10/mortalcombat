import { inject, injectable } from 'inversify';
import { TYPES } from '../config/types';
import { ICharacterDatabase, IEngine, IOpponentsFactory, IOptionsRound, IStartStage } from '../contracts';
import { data } from '../database/data';

@injectable()
export class Engine implements IEngine {

  constructor(
    @inject(TYPES.opponentsFactory) private opponentsFactory: IOpponentsFactory,
    @inject(TYPES.startStage) private startStage: IStartStage,
    @inject(TYPES.optionsRound) private optionsRound: IOptionsRound
  ) {}

  public async start(): Promise<void> {
    this.opponentsFactory.generateOpponents(data);
    await this.startStage.start();
    await this.optionsRound.start();
  }
}
