import { inject, injectable } from 'inversify';
import { TYPES } from '../../config/types';
import { ICommandProcessor, IReader } from '../../contracts';
import { IStartStage } from '../../contracts/engines/IStartStage';

@injectable()

export class StartStage implements IStartStage {

  constructor(
    @inject(TYPES.reader) private _reader: IReader,
    @inject(TYPES.commandProcessor) private _processor: ICommandProcessor
  ) { }

  public async start(): Promise<void> {

    console.log('Type [create] to create new player:');
    const command: string = 'create'; // A await this._reader.read();
    console.log('Type your name:');
    const name: string = 'niki'; // A await this._reader.read();
    try {
      return this._processor.processCommand(command, name);
    } catch (error) {
      return error.message;
    }
  }
}
