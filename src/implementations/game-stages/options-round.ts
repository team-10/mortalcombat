import { inject, injectable, optional } from 'inversify';
import { container } from '../../config/ioc.config';
import { TYPES } from '../../config/types';
import { ICommandProcessor, IFightRound, IOptionsRound, IState } from '../../contracts';
import { ICharacterDatabase } from '../../contracts/ICharacterDatabase';
import { IReader } from '../../contracts/providers/IReader';
import { IWriter } from '../../contracts/providers/IWriter';
@injectable()

export class OptionsRound implements IOptionsRound {

  constructor(
    @inject(TYPES.reader) private reader: IReader,
    @inject(TYPES.database) private database: ICharacterDatabase,
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.writer) private writer: IWriter,
    @inject(TYPES.commandProcessor) private processor: ICommandProcessor
  ) {}

  public async start(): Promise<void> {

    let command : string;
    let optionalParameter: string;

    this.writer.write('Options round - decide what you want to do.');
    while (this.database.opponents.length && this.state.player.isAlive()) {
      do {
        command = await this.reader.read(`Type ${this.state.activeCommandNames.join(' or ')}: `);
        if (!this.state.activeCommandNames.includes(command)) {
          this.writer.write('Invalid command!');
        }
      } while (!this.state.activeCommandNames.includes(command));

      if (command === 'choose') {
        this.processor.processCommand(command);
        command = 'pick';
        do {
          optionalParameter = await this.reader.read('Type number of hero: ');
          if (Number(optionalParameter) < 1 ||
              Number(optionalParameter) > this.state.selectedOpponents.length ||
              isNaN(Number(optionalParameter))) {
            this.writer.write('Invalid number!');
          }
        } while (Number(optionalParameter) < 1 ||
                 Number(optionalParameter) > this.state.selectedOpponents.length ||
                 isNaN(Number(optionalParameter))
        );
        this.processor.processCommand(command, optionalParameter);
        await this.fightOpponent();
      } else {
        this.processor.processCommand(command, optionalParameter);
      }
    }
    if (this.database.opponents.length === 0) {
      this.writer.write('You Won!!!');
    }
  }
  // Real Version of function
  private async fightOpponent(): Promise<void> {
    const fightRound: IFightRound = container.get<IFightRound>(TYPES.fightRound);
    await fightRound.start();
    this.state.restsInRow = 0;
  }
}
