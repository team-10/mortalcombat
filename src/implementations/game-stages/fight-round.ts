import { inject, injectable } from 'inversify';
import { TYPES } from '../../config/types';
import { ICharacterDatabase, ICommandProcessor, IFightRound,
   IOpponent, IPlayer, IReader, IState, IWriter } from '../../contracts';

@injectable()

export class FightRound implements IFightRound {

  private _player: IPlayer;
  private _opponent: IOpponent;

  constructor(
    @inject(TYPES.reader) private reader: IReader,
    @inject(TYPES.database) private database: ICharacterDatabase,
    @inject(TYPES.state) private state: IState,
    @inject(TYPES.writer) private writer: IWriter,
    @inject(TYPES.commandProcessor) private processor: ICommandProcessor
  ) {
    this._player = this.state.player;
    this._opponent = this.state.currentOpponent;
  }

  public async start(): Promise<void> {

    console.log(`\nFIGHTROUND: ${this._player.name} vs. ${this._opponent.name}`);
    while (this._player.isAlive() && this._opponent.isAlive()) {
      // tslint:disable-next-line:max-line-length
      this.writer.write(`${this.state.player.name}(${this.state.player.vitality}) ${this.state.currentOpponent.name}(${this.state.currentOpponent.vitality})`);
      const line: string = await this.reader.read(`${this.state.activeCommandNames.join(' or ')}: `);
      if (this.state.activeCommandNames.includes(line)) {
        this.processor.processCommand(line);
        // A this.writer.write(`You have ${this._player.vitality} hp`);
        // A this.writer.write(`Your opponent has ${this._opponent.vitality} hp`);
      } else {
        this.writer.write(`\ninvalid command`);
      }
    }
    if (!this._player.isAlive()) {
        console.log('\nSadly your brains now cover the floor...GAME OVER');
    } else {
      this.writer.write(`\nYou have defeated ${this._opponent}`);
      const deadOpponentIndex: number = this.database.opponents.indexOf(this._opponent);
      this.database.opponents.splice(deadOpponentIndex , 1);
      // Optionally inject dice and increment inteligence OR strength at random
      this.state.player.inteligence += 1;
      this.state.player.strength += 1;
      this.writer.write(
        `After the fight you have` +
        ` (s:${this._player.strength})(i:${this._player.inteligence})(v:${this._player.vitality}/100)`
      );
    }
  }
}
