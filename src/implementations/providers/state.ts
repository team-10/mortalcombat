import { injectable } from 'inversify';
import { IOpponent, IPlayer } from '../../contracts';
import { IState } from '../../contracts/providers/IState';
@injectable()
export class State implements IState {
  private _player: IPlayer;
  private _currentOpponent: IOpponent;

  private _selectedOpponenets: IOpponent[];
  private _activeCommandNames: string[];
  private _restsInRow: number;

  constructor () {
    this._selectedOpponenets = [];
    this._activeCommandNames = [];
    this._restsInRow = 0;
  }
  public get selectedOpponents(): IOpponent[] {
    return this._selectedOpponenets;
  }
  public set selectedOpponents(value: IOpponent[]) {
    this._selectedOpponenets = value;
  }

  public get player(): IPlayer {
    return this._player;
  }
  public set player(value: IPlayer) {
    this._player = value;
  }
  public get activeCommandNames(): string[] {
    return this._activeCommandNames;
  }
  public set activeCommandNames(value: string[]) {
    this._activeCommandNames = value;
  }
  public get currentOpponent(): IOpponent {
    return this._currentOpponent;
  }
  public set currentOpponent(value: IOpponent) {
    this._currentOpponent = value;
  }
  public get restsInRow(): number {
    return this._restsInRow;
  }
  public set restsInRow(value: number) {
    this._restsInRow = value;
  }
}
