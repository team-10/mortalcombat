import { injectable } from 'inversify';
import { IWriter } from '../../contracts';

@injectable()
export class ConsoleWriter implements IWriter {
  public write(line: string): void {
    console.log(line);
  }
}
