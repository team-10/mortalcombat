import { inject, injectable } from 'inversify';
import { TYPES } from '../../config/types';
import { ICommand, ICommandFactory, ICommandProcessor } from '../../contracts';

@injectable()

export class CommandProcessor implements ICommandProcessor {
  private _commandFactory: ICommandFactory;

  constructor(@inject(TYPES.commandFactory) commandFactory: ICommandFactory) {
    this._commandFactory = commandFactory;
  }

  public processCommand(commandAsString: string, commandParameters?: string): void {
    const command: ICommand = this.parseCommand(commandAsString);

    return command.execute(commandParameters);
  }

  private parseCommand(commandAsString: string): ICommand {
    const commandName: string = commandAsString.trim();

    return this._commandFactory.getCommand(commandName);
  }

}
