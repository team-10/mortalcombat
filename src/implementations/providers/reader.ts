import { injectable } from 'inversify';
import * as readline from 'readline-sync';
import { IReader } from '../../contracts';

@injectable()

export class ConsoleReader implements IReader {

  public async read(path?: string): Promise<string> {
    const msg: string = 'Input here: ';

    return readline.question(path);

  }
}
