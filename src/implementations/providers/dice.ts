import { Chance } from 'chance';
import { injectable } from 'inversify';
import { IDice } from '../../contracts';

@injectable()

export class Dice implements IDice {

  // tslint:disable-next-line:no-any
  private _dice: any = new Chance();

  public roll(min: number, max: number): number {
    return this._dice.integer({min, max});
  }
}
