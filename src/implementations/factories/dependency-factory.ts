import { injectable } from 'inversify';
import { ConsoleWriter, Dice } from '..';
import {IDependencyFactory, IDice, IPlayer, IWriter } from '../../contracts';
import { Player } from '../../models';

@injectable()
export class DependencyFactory implements IDependencyFactory {
  public createPlayer(name: string, strength: number, vitality: number): IPlayer {
    return new Player(name, strength, vitality);
  }
  public createWriter(): IWriter {
    return new ConsoleWriter();
  }
  public createDice(): IDice {
    return new Dice();
  }
}
