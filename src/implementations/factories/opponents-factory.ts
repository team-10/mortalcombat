import { inject, injectable } from 'inversify';
import { TYPES } from '../../config/types';
import { ICharacterDatabase, IDice, IOpponent, ISeedEntry } from '../../contracts';
import { IOpponentsFactory } from '../../contracts/factories/IOpponentsFactory';
import { Opponent } from '../../models';

@injectable()
export class OpponentsFactory implements IOpponentsFactory {
  private _database: ICharacterDatabase;
  private _dice: IDice;
  public constructor(
    @inject(TYPES.database) database: ICharacterDatabase,
    @inject(TYPES.dice) dice: IDice
  ) {
    this._database = database;
    this._dice = dice;
  }
  public generateOpponents(heroes: ISeedEntry[]): void { // There must be a better way
    heroes.forEach((object: ISeedEntry) => {
      const opponent: IOpponent = new Opponent(
        object.name,
        object.strength,
        object.vitality,
        this._dice.roll(1, 6)
      );
    this._database.opponents.push(opponent);
    });
  }
}
