import { inject, injectable } from 'inversify';
import * as commands from '../../commands';
import { TYPES } from '../../config/types';
import { ICharacterDatabase, ICommand, ICommandFactory, IDependencyFactory, IState } from '../../contracts';

@injectable()
export class CommandFactory implements ICommandFactory {
  private readonly _data: ICharacterDatabase;
  private readonly _factory: IDependencyFactory;
  private readonly _state: IState;
  private readonly _commands: Map<string, new (state: IState) => ICommand>;

  public constructor(
    @inject(TYPES.state) state: IState,
    @inject(TYPES.dependencyFactory) factory: IDependencyFactory,
    @inject(TYPES.database) data: ICharacterDatabase
  ) {
    this._data = data;
    this._state = state;
    this._factory = factory;
    this._commands = Object
      .keys(commands)
      .reduce((allCommands: Map<string, new () => ICommand>, commandName: string): Map<string, new () => ICommand> => {
        // tslint:disable:no-any
        allCommands.set(commandName.toLowerCase(), (<any>commands)[commandName]);

        return allCommands;
      },
      new Map()
    );
  }

  public getCommand(commandName: string): ICommand {
    const lowerCaseCommandName: string = commandName.toLowerCase();

    const command: new (
      state: IState,
      factory: IDependencyFactory,
      data: ICharacterDatabase
    ) => ICommand = this._commands.get(lowerCaseCommandName);

    if (!command) {
      throw new Error('Invalid command');
    }

    return new command(this._state, this._factory, this._data);
  }
}
