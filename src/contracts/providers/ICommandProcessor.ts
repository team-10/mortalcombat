
export interface ICommandProcessor {
  processCommand(commandName: string, commandParameters?: string): void;
}
