export interface IDice {
  roll(min: number, max: number): number;
}
