import { IOpponent, IPlayer } from '..';

export interface IState {
  selectedOpponents: IOpponent[];
  player: IPlayer;
  activeCommandNames: string[];
  currentOpponent: IOpponent;
  restsInRow: number;
}
