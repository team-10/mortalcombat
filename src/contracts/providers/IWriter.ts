export interface IWriter {
  write(line: string): void;
}
