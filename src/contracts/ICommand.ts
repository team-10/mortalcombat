export interface ICommand {
  execute(parameters?: string): void;
}
