export interface ISeedEntry {
  name: string;
  strength: number;
  vitality: number;
}
