export interface ITypes {
  // Engines
  engine: symbol;
  startStage: symbol;
  fightRound: symbol;
  optionsRound: symbol;

  // Factories
  dependencyFactory: symbol;
  commandFactory: symbol;
  containerCommandFactory: symbol;
  opponentsFactory: symbol;

  // Providers
  commandProcessor: symbol;
  dice: symbol;
  reader: symbol;
  writer: symbol;
  state: symbol;

  database: symbol;
  player: symbol;

  // Commands
  attack: symbol;
  cast:  symbol;
  choose: symbol;
  create: symbol;
  pick:  symbol;
  rest: symbol;

  [key: string]: symbol;
}
