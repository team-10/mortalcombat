
import { ICharacter } from './ICharacter';

export interface IOpponent extends ICharacter {
  id: number;
}
