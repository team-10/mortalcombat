export interface ICharacter {
  name: string;
  strength: number;
  vitality: number;
  inteligence: number;
  minDmg: number;
  maxDmg: number;
  canCast(): boolean;
  spellDmg(): number;
  isAlive(): boolean;
  toString(): string;
}
