import { ICharacter } from './ICharacter';

export interface IPlayer extends ICharacter {
  increaseStrength(x: number): void;
}
