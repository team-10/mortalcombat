import { IEngine } from '.';

export interface IStartStage extends IEngine {
  start(): Promise<void>;
}
