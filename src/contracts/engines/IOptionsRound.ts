import { IEngine } from '.';

export interface IOptionsRound extends IEngine {
  start(): void;
}
