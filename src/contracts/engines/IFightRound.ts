import { IEngine } from '.';

export interface IFightRound extends IEngine {
  start(): void;
}
