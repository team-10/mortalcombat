import { IOpponent } from '.';
import { ISeedEntry } from './ISeedEntry';

export interface ICharacterDatabase {
  opponents: IOpponent[];
}
