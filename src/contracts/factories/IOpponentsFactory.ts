import { ISeedEntry } from '..';

export interface IOpponentsFactory {
  generateOpponents(heroes: ISeedEntry[]): void;
}