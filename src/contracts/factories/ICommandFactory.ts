import { ICommand } from '../ICommand';

export interface ICommandFactory {
  getCommand(commandName: string): ICommand;
}
