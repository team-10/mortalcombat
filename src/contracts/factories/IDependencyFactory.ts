import { IDice, IPlayer, IWriter } from '..';

export interface IDependencyFactory {
  createPlayer(name: string, strength: number, vitality: number): IPlayer;
  createDice(): IDice;
  createWriter(): IWriter;
}
