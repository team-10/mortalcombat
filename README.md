# Mortal Combat

## Contributors:
Team 10

Team leader: Niki

Team members: [Nikolai](https://my.telerikacademy.com/Users/hldd3n), [Nikola](https://my.telerikacademy.com/Users/nizlatinov)

## Project Description:
A console game application providing players the opportunity to test their skills and luck against opponents with purely coincidential resemblence to actual persons.

## Project Features:

 ### Actors (Hero and Opponents)

 Hero and Opponents have :
  - vitality (with maximum of 100)
  - strength
  - inteligence

 They can both :
  - attack
  - cast
  - die

 Hero has the ability to rest (restore vitality).
 After a victory the inteligence and strength of the hero are incremented.

 The game ends when all opponents are defeated or the hero dies.

 The game can be broken down to the following stages:

 ### Player Creation
 - A stage where the name of the hero can be choosen

 ### Options Round
 - Provides the option to 'rest' or 'choose' an opponent
 - Porvides a list of current opponents once 'choose' has been typed

 ### Fight Round
 - Gives the stage of the confrontation between the hero and the choosen opponent
 - The hero has the ability to attack the opponent
 - If the hero is inteligent enought he/she will have the option

## Trello:
[Board](https://trello.com/b/vQNayXDj/mortal-combat)
[Repository](https://gitlab.com/team-10/mortalcombat)