import { ICharacter } from '../../../src/contracts';
import { Character } from '../../../src/models';

export class MockCharacter extends Character implements ICharacter {
  constructor(name: string, strength: number, vitality: number) {
    super(name, strength, vitality);
    this.inteligence = 4;
  }
  protected enhancement(): number {
    return 4;
  }
}
