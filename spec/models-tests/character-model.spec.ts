// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { ICharacter } from '../../src/contracts';
import { MockCharacter } from './abstractions/mockCharacter-class';

describe('Character class', () => {
  describe('constructor should ', () => {
    it('correctly assign passed values', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act && Assert
      expect(character.name).toBe('Ivan');
      expect(character.strength).toBe(5);
      expect(character.vitality).toBe(100);
      expect(character.inteligence).toBe(4);
    });
    it ('trow when input in setters is incorrect', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act && Assert
      expect(() => {character.name = 'a'; }).toThrow();
      expect(() => {character.name = 'a'.repeat(20); }).toThrow();
      expect(() => {character.strength = 0; }).toThrow();
      expect(() => {character.vitality = -1; }).toThrow();
      expect(() => {character.vitality = 101; }).toThrow();
      expect(() => {character.inteligence = 0; }).toThrow();
    });
  });
  describe('spellDmg() should', () => {
    it('return correct spellDmg ammount', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act // Arrange
      expect(character.spellDmg()).toBe(20);
    });
  });
  describe('canCast() should', () => {
    it('return true when character inteligence is greater than 4', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act
      character.inteligence = 5;
      // Assert
      expect(character.canCast()).toBeTruthy();
    });
    it('return false when character inteligence is less than 5', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act // Assert
      expect(character.canCast()).toBeFalsy();
    });
  });
  describe('isAlive() should ', () => {
    it('return true when character vitality is positive', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);
      // Act Assert
      expect(character.isAlive()).toBeTruthy();
    });
    it('return false when character vitality is 0', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 0);
      // Act Assert
      expect(character.isAlive()).toBeFalsy();
    });
  });
  describe('toString() should', () => {
    it('display instance data correctly', () => {
      // Arrange
      const character: ICharacter = new MockCharacter('Ivan', 5, 100);

      // Act
      const result: string = character.toString();

      // Assert
      expect(result).toBe('Ivan(s:5)(i:4)(v:100/100)');
    });
  });
});
