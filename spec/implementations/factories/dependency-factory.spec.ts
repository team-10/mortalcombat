// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { ICharacter, IDependencyFactory, IDice, IWriter } from '../../../src/contracts';
import { ConsoleWriter, DependencyFactory, Dice } from '../../../src/implementations';
import { Player } from '../../../src/models';

describe('Dependency factory', () => {
  describe('createPlayer should', () => {
    it('throw when name is an empty string', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('', 50, 100)).toThrow(`Player's name should be between 3 and 16 symbols long!`);
    });
    it('throw when name is less than 3 symbols', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('t', 50, 100)).toThrow(`Player's name should be between 3 and 16 symbols long!`);
    });
    it('throw when name is more than 16 symbols', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('t'.repeat(17), 50, 100)).toThrow(`Player's name should be between 3 and 16 symbols long!`);
    });
    it('throw when strength is less than 1', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('Test', 0, 100)).toThrow(`Player's strength should be at least one.`);
    });
    it('throw when vitality is less than 0', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('Test', 5, -1)).toThrow(`Player's vitality should be between 0 and 100!`);
    });
    it('throw when vitality is more than 100', () => {
      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act & Assert
      expect(() => factory.createPlayer('Test', 5, 101)).toThrow(`Player's vitality should be between 0 and 100!`);
    });
    it('create new Player with correct name,strength and vitality', () => {
       // Arrange
       const factory: IDependencyFactory = new DependencyFactory();
       // Act
       const player: ICharacter = factory.createPlayer('test', 5, 100);
      // Assert
      expect(player).toBeInstanceOf(Player);
      expect(player.name).toBe('test');
      expect(player.strength).toBe(5);
      expect(player.vitality).toBe(100);
    });
  });
  describe('createWriter shoud', () => {
    it('create instance of ConsoleWriter', () => {

      // Arrange
      const factory: IDependencyFactory = new DependencyFactory();
      // Act
      const writer: IWriter = factory.createWriter();
      // Assert
      expect(writer).toBeInstanceOf(ConsoleWriter);

    });
  });
  describe('createDice should', () => {
    it('create instance of Dice', () => {

            // Arrange
            const factory: IDependencyFactory = new DependencyFactory();
            // Act
            const dice: IDice = factory.createDice();
            // Assert
            expect(dice).toBeInstanceOf(Dice);

    });
  });
});
