// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { ICharacterDatabase, ICommandProcessor, IOptionsRound, IReader, IState, IWriter } from '../../../src/contracts';
import { OptionsRound } from '../../../src/implementations';

describe('OptionsRound', () => {
  describe('start should', () => {
    let mockReader: jest.Mock<IReader>;
    let mockWriter: jest.Mock<IWriter>;
    let mockDatabase: jest.Mock<ICharacterDatabase>;
    let mockState: jest.Mock<IState>;
    let mockCommandProcessor: jest.Mock<ICommandProcessor>;

    let spy: jest.SpyInstance;

    beforeEach(() => {
      mockReader = jest.fn<IReader>().mockImplementation(() => ({
        read: jest.fn().mockReturnValue(Promise.resolve(['inputValue']))
      }));

      mockWriter = jest.fn<IWriter>().mockImplementation(() => ({
        write: jest.fn()
      }));

      mockDatabase = jest.fn<ICharacterDatabase>().mockImplementation(() => ({
        opponents: []
      }));

      mockState = jest.fn<IState>().mockImplementation(() => ({
        player: {
          isAlive: (): boolean => false
        }
      }));

      mockCommandProcessor = jest.fn<ICommandProcessor>().mockImplementation(() => ({
        processCommand: jest.fn().mockReturnValue('command result')
      }));

    });

    afterEach(() => {
      spy.mockRestore();
    });

    it ('call writer with "You Won!!!" if there are no more opponents', async () => {
      // Arrange
      const reader: IReader = new mockReader();
      const database: ICharacterDatabase = new mockDatabase();
      const state: IState = new mockState();
      const writer: IWriter = new mockWriter();
      const processor: ICommandProcessor = new mockCommandProcessor();
      const optionsRound: IOptionsRound = new OptionsRound(reader, database, state, writer, processor);

      spy = jest.spyOn(writer, 'write');
      // Act
      await optionsRound.start();

      // Assert
      expect(spy).toBeCalledWith('You Won!!!');
    });

    it ('not call reader if there are no more opponents', async () => {
      // Arrange
      const reader: IReader = new mockReader();
      const database: ICharacterDatabase = new mockDatabase();
      const state: IState = new mockState();
      const writer: IWriter = new mockWriter();
      const processor: ICommandProcessor = new mockCommandProcessor();
      const optionsRound: IOptionsRound = new OptionsRound(reader, database, state, writer, processor);

      spy = jest.spyOn(reader, 'read');
      // Act
      await optionsRound.start();

      // Assert
      expect(spy).toBeCalledTimes(0);
    });

    it ('not call reader if player is dead', async () => {
      // Arrange
      const reader: IReader = new mockReader();
      const database: ICharacterDatabase = new mockDatabase();
      const state: IState = new mockState();
      const writer: IWriter = new mockWriter();
      const processor: ICommandProcessor = new mockCommandProcessor();
      const optionsRound: IOptionsRound = new OptionsRound(reader, database, state, writer, processor);

      spy = jest.spyOn(reader, 'read');

      // Act
      await optionsRound.start();

      // Assert
      expect(spy).not.toBeCalled();
      });
      /*
      Uncomment when while loop logic is moved from optionsRound to engine
      it ('call reader if player is not dead and there are opponents left', async () => {
      // Arrange

      mockState = jest.fn<IState>().mockImplementation(() => ({
        player: {
          isAlive: (): boolean => true
        },
        activeCommandNames: ['command1', 'command2']
      }));
      const reader: IReader = new mockReader();
      const database: ICharacterDatabase = new mockDatabase();
      database.opponents.length = 2;
      const state: IState = new mockState();
      const writer: IWriter = new mockWriter();
      const processor: ICommandProcessor = new mockCommandProcessor();
      const optionsRound: IOptionsRound = new OptionsRound(reader, database, state, writer, processor);
      spy = jest.spyOn(reader, 'read');

      // Act
      await optionsRound.start();

      // Assert
      expect(spy).toBeCalled();
    });
    */
  });
});
