// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { IDice } from '../../../src/contracts';
import { Dice } from '../../../src/implementations';

describe('Dice', () => {
  describe('roll should', () => {
    it ('return value from the passed range', () => {
      // Arrange
      const dice: IDice = new Dice();
      // Act;
      const result: number = dice.roll(1, 4);
      // Assert
      expect(result).toBeGreaterThanOrEqual(1);
      expect(result).toBeLessThanOrEqual(4);
    });
  });
});
