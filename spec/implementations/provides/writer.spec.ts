// tslint:disable-next-line:no-import-side-effect
import 'reflect-metadata';
import { IWriter } from '../../../src/contracts';
import { ConsoleWriter } from '../../../src/implementations';

describe('Writer', () => {
  describe('write should', () => {
    it ('call the log method of the console with the passed argument', async () => {
      // Arrange
      const writer: IWriter = new ConsoleWriter();
      const consoleSpy: jest.SpyInstance = jest.spyOn(console, 'log');
      // Act;
      writer.write('TEST');
      // Assert
      expect(consoleSpy).toBeCalledWith('TEST');
    });
  });
});
